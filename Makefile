CC      =  g++
SRCS    =  src/*.c
OBJS    =  src/*.o
EXECS   =  classify lmclus prepro
CFLAGS  =  -Wall -g -Ih

.PHONY: all clean cleanall

all: $(EXECS)

clean:
	-rm -f $(OBJS)

cleanall:
	-rm -f $(OBJS) $(EXECS)

.c:
	$(CC) $(CFLAGS) $@.c -o $@
