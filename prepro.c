#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

//main entry point for this program
//usage prepro [datafile] 
int main(int argc, char *argv[])
{
	FILE* file = fopen(argv[1], "r");
	char line[512];
	int first_line = 0;
	fgets ( line, sizeof line, file );
	while(fgets ( line, sizeof line, file ) != NULL)
	{
		char * field;
		char tmp[8];
		field = strtok(&line[0],",");
		field = strtok(NULL,",");
	
		struct tm * timeinfo;
		time_t rawtime;
		time ( &rawtime );
		timeinfo = localtime ( &rawtime );
		
		//set year
		strncpy(tmp, field, 4);		
		timeinfo->tm_year = atoi(tmp) - 1900;
		strncpy(tmp, field + 4, 4);
		tmp[2] = '\0';
		timeinfo->tm_mon = atoi(tmp) - 1;
		strncpy(tmp, field + 6, 4);
		tmp[2] = '\0'; 
		timeinfo->tm_mday = atoi(tmp);
		
		//set time
		field = strtok(NULL,",");
		char field2[strlen(field)];
		strcpy(field2, field);

		//get price
		field = strtok(NULL,",");
		field = strtok(NULL,",");
		field = strtok(NULL,",");
		
		//back to time
		char * sub_field;
		sub_field = strtok(&field2[0], ":.");
		timeinfo->tm_hour = atoi(sub_field);
		sub_field = strtok(NULL, ":.");
		timeinfo->tm_min = atoi(sub_field);
		sub_field = strtok(NULL, ":.");
		timeinfo->tm_sec = atoi(sub_field);
		sub_field = strtok(NULL, ":.");
		
		//set milli
		char milli[strlen(sub_field)];
		strcpy(milli, sub_field);
		time_t time_entry = mktime ( timeinfo );
		
		field[strlen(field) - 1] = '\0';
		
		if (first_line == 0)
			first_line = (int)(time_entry);
		printf("%d.%s,%s\n", (int)(time_entry - first_line), milli,field);
		
		//if (strcmp( curr, last ) != 0)
		//	printf("%s\n", curr);
		//strcpy(last, curr);
	}
	return 0;
}