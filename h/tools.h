#ifndef TOOLS_H
#define TOOLS_H

#include <math.h>

#define DEBUG_ON 0
#define MAX_VAL 0xFFFFFFFF

#define MAX_DP_SIZE 10000
#define MAX_CL_SIZE MAX_DP_SIZE

#ifndef EPSILON
#define EPSILON 0.2
#endif

//since I only use financial data this function only works for R^2
//but it could be generalized to work with any size space
void FormOrthonormalBasis(Point* _col_vec, Point _org, int _size, Point *&result)
{
	if (DEBUG_ON >= 1)
	{
		printf("\nRun FormOrthonormalBasis. \n");
		printf("  Origin = %f , %f\n",_org.dim_1, _org.dim_2);
		printf("  Size = %d\n", _size );
	}

	int i;
	result = new Point[_size];
	double denom = _org.dim_1 * _org.dim_1
					+ _org.dim_2 * _org.dim_2;
	for (i = 0; i < _size; i++)
	{
		double numer = _org.dim_1 * _col_vec[i].dim_1
					+ _org.dim_2 * _col_vec[i].dim_2; 
		result[i].dim_1 = _col_vec[i].dim_1 
				- ((numer / denom) *_org.dim_1);
		result[i].dim_2 = _col_vec[i].dim_2 
				- ((numer / denom) *_org.dim_2);
		//Normalize vector
		double v2 = sqrt(result[i].dim_1  * result[i].dim_1 
						+ result[i].dim_2  * result[i].dim_2);
						
		if (v2 != 0)
		{
			result[i].dim_1 *= (1/v2);
			result[i].dim_2 *= (1/v2);
		}
		if (DEBUG_ON >= 1)
		{
			printf("  Beta point %d. %f , %f\n", i, result[i].dim_1, result[i].dim_2);
			printf("  Col Vec point %d. %f , %f\n", i, _col_vec[i].dim_1, _col_vec[i].dim_2);
		}
	}
	if (DEBUG_ON >= 1)
		printf("Done FormOrthonormalBasis. \n");
	
	return;
}

void MakeHist(double * _data, int _size, int _bins, Bin *&result)
{
	if (DEBUG_ON >= 1)
	{
		printf("\nRun MakeHist. \n");
		printf("  Bins = %d\n", _bins );
		printf("  Size = %d\n", _size );
	}
	int i,j;
	double min = _data[0];
	double max = min;
	result = new Bin[_bins];
	for (i = 0; i < _size; i++)
	{
		//printf("  %d : %f \n", i, _data[i]);
		if (_data[i] < min)
			min = _data[i];
		if (_data[i] > max)
			max = _data[i];
	}
	double bin_width = (max - min) / _bins;
	if (DEBUG_ON >= 1)
		printf("  Bid Width = %f\n", bin_width);
		
	for (i = 0; i < _bins; i++)
	{
		result[i].floor = min + i * bin_width;
		result[i].ceiling = min + (i + 1) * bin_width;
		result[i].count = 0;
		
	}

	for (i = 0; i < _size; i++)
	{
		for (j = 0; j < _bins; j++)
		{
			if (_data[i] >= result[j].floor
				&& _data[i] <= result[j].ceiling)
				result[j].count++;
		}
	}
	if (DEBUG_ON >= 2)
	{
		for(i = 0; i< _bins; i++)
			printf ("  %d:(%.2f-%.2f),%d\n", i, result[i].floor, result[i].ceiling, result[i].count );
	}
	if (DEBUG_ON >= 1)
		printf("Done MakeHist. \n");
	return;
}

//test all possible thresholds to find min
ErrorThres FindMinErThres(Bin * _hist, int _size)
{
	if (DEBUG_ON >= 1)
	{
		printf("\nRun FindMinErThres. \n");
		printf("  Size = %d\n", _size );
	}

	ErrorThres result;
	result.Jt = 99999999;
	int i, j, s2;
	for (i = 1; i < _size - 1; i++)
	{
		int P1 = 0;
		int P2 = 0;
		double mean1 = 0;
		double mean2 = 0;
		double sigma1 = 0;
		double sigma2 = 0;
		
		for (j = 0; j < i; j++)
		{
			P1 += _hist[j].count;
			mean1 += _hist[j].count * ((_hist[j].floor + _hist[j].ceiling) / 2);
		}
		mean1 /= P1;
		for (j = 0; j < i; j++)
			sigma1 += pow (((_hist[j].floor + _hist[j].ceiling) / 2) - mean1 , 2) * _hist[j].count;
		sigma1 /= P1;
		sigma1 = sqrt(sigma1);
		
		for (j = i; j < _size; j++)
		{
			P2 += _hist[j].count;
			mean2 += _hist[j].count * ((_hist[j].floor + _hist[j].ceiling) / 2);
		}
		mean2 /= P2;
		for (j = i; j < _size; j++)
			sigma2 += pow (((_hist[j].floor + _hist[j].ceiling) / 2) - mean2 , 2) * _hist[j].count;
		sigma2 /= P2;
		sigma2 = sqrt(sigma2);
		
		if (sigma1 == 0)//check for sigmas = 0;
			sigma1 = 1;
		if (sigma2 == 0)//check for sigmas = 0;
			sigma2 = 1;
			
		double tmp = 1 + 2 * (P1 * log(sigma1) + P2 * log(sigma2))
					- 2 * (P1 * log(P1) + P2 * log(P2));
					
		if (tmp < result.Jt)
		{
			result.Jt = tmp;
			result.thres = ((_hist[i].floor + _hist[i].ceiling) / 2);
			s2 = i;
			result.mean1 = mean1;
			result.mean2 = mean2;
			result.sigma1 = sigma1;
			result.sigma2 = sigma2;
		}
	}
	result.Jt_pr = result.Jt;
	
	for (i = s2; i < _size; i++)
	{
		int P1 = 0;
		int P2 = 0;
		double mean1 = 0;
		double mean2 = 0;
		double sigma1 = 0;
		double sigma2 = 0;
		
		for (j = 0; j < i; j++)
		{
			P1 += _hist[j].count;
			mean1 += _hist[j].count * ((_hist[j].floor + _hist[j].ceiling) / 2);
		}
		mean1 /= P1;
		for (j = 0; j < i; j++)
			sigma1 += pow (((_hist[j].floor + _hist[j].ceiling) / 2) - mean1 , 2) * _hist[j].count;
		sigma1 /= P1;
	
		for (j = i; j < _size; j++)
		{
			P2 += _hist[j].count;
			mean2 += _hist[j].count * ((_hist[j].floor + _hist[j].ceiling) / 2);
		}
		mean2 /= P2;
		for (j = i; j < _size; j++)
			sigma2 += pow (((_hist[j].floor + _hist[j].ceiling) / 2) - mean2 , 2) * _hist[j].count;
		sigma2 /= P2;
		
		if (sigma1 == 0)//check for sigmas = 0;
			sigma1 = 1;
		if (sigma2 == 0)
			sigma2 = 1;
			
		double tmp = 1 + 2 * (P1 * log(sigma1) + P2 * log(sigma2))
					- 2 * (P1 * log(P1) + P2 * log(P2));
					
		if (tmp > result.Jt_pr)
			result.Jt_pr = tmp;
	
	}
	if (DEBUG_ON >= 1)
		printf("Done FindMinErThres. \n");
	return result;
}
 
double EvalGoodness(ErrorThres _err_thres, Bin * _hist)
{
	if (DEBUG_ON >= 1)
	{
		printf("\nRun EvalGoodness. \n");
		printf("  Mean1 = %f\n", _err_thres.mean1 );
		printf("  Mean2 = %f\n", _err_thres.mean2 );
		printf("  Sig1 = %f\n", _err_thres.sigma1 );
		printf("  Sig2 = %f\n", _err_thres.sigma2 );
		printf("  JT = %f\n", _err_thres.Jt );	
		printf("  JT' = %f\n", _err_thres.Jt_pr  );
	}
	double result = 0;
	result = pow(_err_thres.mean1 - _err_thres.mean2, 2);
	result /= pow(_err_thres.sigma1, 2) + pow(_err_thres.sigma2, 2);
	result *= _err_thres.Jt_pr - _err_thres.Jt;
	
	if (DEBUG_ON >= 1)
	{
		printf("  Goodness = %f\n", result);
		printf("Done EvalGoodness. \n");
	}
	return result;
}

//generates econmic gain
double** GenEcomGain(int nc) 
{
	double **result;
	result = new double*[nc];
	for (int i = 0; i < nc; ++i)
    	result[i] = new double[nc];
		
	for (int i = 0; i < nc; ++i)	
		for (int j = 0; j < nc; ++j)
			if (j == i)
				result[i][j] = rand() % 10;
		
	return result;	
}

double* GenBayesDes(double **prob, double **ecomGain, int nd, int nc)
{
	double *result = new double[nd];
	
	for (int i = 0; i < nd; ++i)	
	{
		double max = 0;
		int inx = 0;
		for (int j = 0; j < nc; ++j)
		{
			if ((prob[i][j] * ecomGain[j][j]) > max)
			{
				max = prob[i][j] * ecomGain[j][j];
				inx = j;
			}
		}
		result[i] = inx;
	}
	
	return result;
}

double** GenConf(double **prob, double *bayes, int nd, int nc)
{
	double **result;
	result = new double*[nc];
	for (int i = 0; i < nc; ++i)
    	result[i] = new double[nc];
		
	for (int i = 0; i < nd; ++i)	
	{		
		for (int j = 0; j < nc; ++j)
		{
			double sum = 0;
			for (int k = 0; k < nd; ++k)			
				if (i == bayes[k])
					sum += prob[i][j];
			result[i][j] = sum;
		}
	}
		
	return result;	
}

double ExpGain(double **ecomGain, double **conf, int nd, int nc)
{
	double sum = 0;
	for (int i = 0; i < nd; ++i)	
		for (int j = 0; j < nc; ++j)
			 sum += ecomGain[i][j] * conf[i][j];
		
	return sum;		
}

//beta calculation
double BetaCalc(Point * _array1, double * _array2, int _size)
{
	double rr1[_size - 1];
	double mrr1 = 0;
	double rr2[_size - 1];
	double mrr2 = 0;
	double rr12[_size - 1];
	double mrr12 = 0;
	int i;
	for (i = 0; i < _size - 1; i++) // rate of return
	{
		rr1[i] = (_array1[i + 1].dim_2 - _array1[i].dim_2 ) / _array1[i].dim_2 ;
		mrr1 += rr1[i];
		rr2[i] = (_array2[i + 1] - _array2[i] ) / _array2[i] ;
		mrr2 += rr2[i];
		rr12[i] = rr1[i] * rr2[i];
		mrr12 += rr12[i];
		//printf("%f(%f), %f(%f), %f\n", rr1[i],_array1[i].dim_2, rr2[i],_array2[i],rr12[i] );
	}
	mrr1 /= _size - 1;
	mrr2 /= _size - 1;
	mrr12 /= _size - 1;
	double mrr1x2 = mrr1 * mrr2;
	double covar = mrr1x2 - mrr12;
	double var = 0;
	for (i = 0; i < _size - 1; i++) // var of rr2
		var += pow(rr2[i] - mrr2, 2);
	var /= _size - 1;
	return covar / var;
}
#endif 
